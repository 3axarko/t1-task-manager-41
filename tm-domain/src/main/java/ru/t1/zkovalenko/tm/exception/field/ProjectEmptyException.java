package ru.t1.zkovalenko.tm.exception.field;

public final class ProjectEmptyException extends AbstractFieldException {

    public ProjectEmptyException() {
        super("Project is empty");
    }

}
