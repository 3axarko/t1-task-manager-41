package ru.t1.zkovalenko.tm.exception.field;

public final class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("Login is empty");
    }

}
