package ru.t1.zkovalenko.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ForeignKey;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUserOwnerModelDTO extends AbstractModelDTO {

    @Nullable
    @Column(nullable = false, name = "user_id")
    private String userId;

}
