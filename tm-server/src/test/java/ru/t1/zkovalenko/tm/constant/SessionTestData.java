package ru.t1.zkovalenko.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.model.SessionDTO;

import java.util.Date;

import static ru.t1.zkovalenko.tm.constant.UserTestData.USER1;

public final class SessionTestData {

    @NotNull
    public final static String SESSION_NAME = "SESSION_NAME";

    @NotNull
    public final static String SESSION_DESCRIPTION = "SESSION_DESCRIPTION";

    @NotNull
    public final static SessionDTO SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO SESSION2 = new SessionDTO();

    static {
        SESSION1.setUserId(USER1.getId());
        SESSION1.setDate(new Date());

    }

}
