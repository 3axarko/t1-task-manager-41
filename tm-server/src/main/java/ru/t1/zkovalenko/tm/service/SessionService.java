package ru.t1.zkovalenko.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.ISessionRepository;
import ru.t1.zkovalenko.tm.api.repository.IUserOwnerRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.ISessionService;
import ru.t1.zkovalenko.tm.dto.model.SessionDTO;

public class SessionService extends AbstractUserOwnerService<SessionDTO> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull IUserOwnerRepository<SessionDTO> getRepository(@NotNull SqlSession connection) {
        return connection.getMapper(ISessionRepository.class);
    }

}
