package ru.t1.zkovalenko.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.repository.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.IUserService;
import ru.t1.zkovalenko.tm.dto.model.UserDTO;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.exception.entity.ProjectNotFoundException;
import ru.t1.zkovalenko.tm.exception.field.IdEmptyException;
import ru.t1.zkovalenko.tm.exception.field.LoginEmptyException;
import ru.t1.zkovalenko.tm.exception.field.PasswordEmptyException;
import ru.t1.zkovalenko.tm.exception.field.RoleEmptyException;
import ru.t1.zkovalenko.tm.exception.user.EmailEmptyException;
import ru.t1.zkovalenko.tm.exception.user.ExistsEmailException;
import ru.t1.zkovalenko.tm.exception.user.ExistsLoginException;
import ru.t1.zkovalenko.tm.exception.user.UserNotFoundException;
import ru.t1.zkovalenko.tm.util.HashUtil;

public final class UserService extends AbstractService<UserDTO> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final SqlSession connection) {
        return connection.getMapper(IProjectRepository.class);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final SqlSession connection) {
        return connection.getMapper(ITaskRepository.class);
    }

    @NotNull
    public IUserRepository getRepository(@NotNull final SqlSession connection) {
        return connection.getMapper(IUserRepository.class);
    }

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user;
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findByLogin(login);
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user;
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findByEmail(email);
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO remove(@Nullable final UserDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        final UserDTO user = super.remove(model);
        final String userId = user.getId();
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final IUserRepository userRepository = getRepository(connection);
            taskRepository.clearByUserId(userId);
            projectRepository.clearByUserId(userId);
            userRepository.remove(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        return remove(user);
    }

    @NotNull
    @Override
    public UserDTO removeByEmail(@Nullable final String email) {
        @NotNull final UserDTO user = findByEmail(email);
        return remove(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO setPassword(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO updateUser(
            @Nullable final String id, @NotNull final String firstName,
            @NotNull final String lastName, @NotNull final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByLogin(login) != null;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new ExistsEmailException();
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByEmail(email) != null;
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final UserDTO user = findByLogin(login);
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final UserDTO user = findByLogin(login);
        user.setLocked(false);
        update(user);
    }

}
