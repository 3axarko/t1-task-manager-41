package ru.t1.zkovalenko.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository extends IUserOwnerRepository<ProjectDTO> {

    @Update("UPDATE tm_project " +
            "SET user_id = #{userId}, " +
            "name = #{name}, " +
            "description = #{description}, " +
            "status = #{status}" +
            " WHERE id = #{id}")
    void update(@NotNull ProjectDTO project);

    @Insert("INSERT INTO tm_project " +
            "(id, created, user_id, name, description, status) " +
            "VALUES (#{id}, #{created}, #{userId}, #{name}, #{description}, #{status})")
    void add(@NotNull ProjectDTO project);

    @Select("SELECT * FROM tm_project order by #{sortField}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<ProjectDTO> findAll(@NotNull String sortField);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} order by #{sortField}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<ProjectDTO> findAllByUserId(@Param("userId") @NotNull String userId, @Param("sortField") @NotNull String sortField);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearByUserId(String userId);

    @Select("SELECT * FROM tm_project WHERE id = #{id} LIMIT 1")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneById(@NotNull String id);

    @Select("SELECT * from (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM tm_project) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIndex(@NotNull Integer index);

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void remove(@NotNull ProjectDTO project);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIdUserId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM tm_project WHERE user_id = #{userId}) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIndexUserId(@Param("userId") @NotNull String userId,
                                              @Param("index") @NotNull Integer index);

}
