package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.Domain;

public interface IDomainService {

    @NotNull Domain getDomain();

    void setDomain(@Nullable Domain domain);

    void dataBackupLoad();

    void dataBackupSave();

    void dataBase64Load();

    void dataBase64Save();

    void dataBinaryLoad();

    void dataBinarySave();

    void dataJsonLoadJaxB();

    void dataJsonLoadJFasterXml();

    void dataJsonSaveFasterXml();

    void dataJsonSaveJaxB();

    void dataXmlLoadJaxB();

    void dataXmlLoadJFasterXml();

    void dataXmlSaveFasterXml();

    void dataXmlSaveJaxB();

    void dataYamlLoadFasterXml();

    void dataYamlSaveFasterXml();

    void dataDisableAutoBackup();

    void dataEnableAutoBackup();

}
