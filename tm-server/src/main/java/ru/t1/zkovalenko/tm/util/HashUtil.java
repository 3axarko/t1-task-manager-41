package ru.t1.zkovalenko.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.component.ISaltProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    static String salt(@NotNull final ISaltProvider saltProvider, @NotNull final String value) {
        @NotNull final String secret = saltProvider.getPasswordSecret();
        @NotNull final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value, secret, iteration);
    }

    @NotNull
    static String salt(
            @NotNull final String value,
            @NotNull final String secret,
            @NotNull final Integer iteration
    ) {
        @Nullable String result = value;
        for (int i = 1; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result == null ? "" : result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (@NotNull NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
